package cl.ciisa.gym;
/**
 * @author Jean Carlos
 * @since 13-06-2021
 * */

public class Calculo {

    private Float altura;
    private Float peso;

    public Calculo(Float altura, Float peso) {
        this.altura = altura / 100;
        this.peso = peso;
    }

    public Float obtenerCalculo() {
        return peso / (altura * altura);
    }

    public Float getAltura() {
        return altura;
    }

    public void setAltura(Float altura) {
        this.altura = altura;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }
}
