package cl.ciisa.gym;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
/**
 * @author Jean Carlos
 * @since 13-06-2021
 * */

public class VerEliminarRecord extends AppCompatActivity {
    private SQLiteDatabase db;
    private Cursor cursor;
    private SQLiteOpenHelper openHelper;
    private Button eliminarBtn;
    private TextView tvListaRecord;
    private TextView fecha, peso, imc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_eliminar_record);
        openHelper = new BD(this);
        db = openHelper.getReadableDatabase();

        Intent intent = getIntent();
        final int position = intent.getIntExtra("position",0);
        final String idPer = intent.getStringExtra("idPer");
        final String idCal = intent.getStringExtra("idCal");
        String fecha = intent.getStringExtra("fecha");
        String peso = intent.getStringExtra("peso");
        String imc = intent.getStringExtra("imc");



        tvListaRecord = findViewById(R.id.tvListaCompras);
        eliminarBtn = findViewById(R.id.btnEliminar);

        final Record record = new Record();
        this.fecha = findViewById(R.id.fecha);
        this.fecha.setText(fecha);
        this.peso = findViewById(R.id.peso);
        this.peso.setText(peso);
        this.imc = findViewById(R.id.imc);
        this.imc.setText(imc);

        record.setFecha(fecha);
        record.setPeso(peso);
        record.setImc(imc);
        record.setIdcalculo(idCal);
        record.setIdpersona(idPer);

        eliminarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder adb = new AlertDialog.Builder(VerEliminarRecord.this);
                adb.setTitle("Eliminar?");
                adb.setMessage("Eliminar Fecha:" + record.fecha + " Peso:" + record.peso + " IMC" + record.imc + " id:" + record.idcalculo);
                adb.setNegativeButton("Cancelar", null);
                adb.setPositiveButton("Eliminar", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String table = BD.TABLE_CALCULOS;
                        String whereClause = "ID=?";
                        String[] whereArgs = new String[]{String.valueOf(record.idcalculo)};
                        db.delete(table, whereClause, whereArgs);
                        EntradaExitosa.registros.clear();
                        EntradaExitosa.adapter.notifyDataSetChanged();
                        Intent intent = new Intent(VerEliminarRecord.this, EntradaExitosa.class);
                        intent.putExtra("nombre", EntradaExitosa.nombre);
                        intent.putExtra("apellido", EntradaExitosa.apellido);
                        intent.putExtra("altura", EntradaExitosa.altura);
                        intent.putExtra("id", EntradaExitosa.idPer);
                        startActivity(intent);
                        finish();
                        Toast.makeText(getApplicationContext(), "Eliminado satisfactoriamente " + record.idcalculo, Toast.LENGTH_SHORT).show();
                    }
                });
                adb.show();
            }
        });


        tvListaRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VerEliminarRecord.this, EntradaExitosa.class);
                intent.putExtra("nombre", EntradaExitosa.nombre);
                intent.putExtra("apellido", EntradaExitosa.apellido);
                intent.putExtra("altura", EntradaExitosa.altura);
                intent.putExtra("id", EntradaExitosa.idPer);
                startActivity(intent);
                finish();
            }
        });
    }


}
