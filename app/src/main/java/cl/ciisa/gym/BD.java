package cl.ciisa.gym;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BD extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "registrogym.db";
    public static final String TABLE_REGISTROS = "registro";
    public static final String TABLE_CALCULOS = "calculos";

    public static final String COL_1 = "ID";
    public static final String COL_2 = "Nombre";
    public static final String COL_3 = "Apellido";
    public static final String COL_4 = "FechaNacimiento";
    public static final String COL_5 = "Altura";
    public static final String COL_6 = "Correo";
    public static final String COL_7 = "Contrasena";

    public static final String COL_CALCULOS_1 = "ID";
    public static final String COL_CALCULOS_2 = "ID_PERSONA";
    public static final String COL_CALCULOS_3 = "Fecha";
    public static final String COL_CALCULOS_4 = "Peso";
    public static final String COL_CALCULOS_5    = "MC";
    /**
     * @author Jean Carlos
     * @since 13-06-2021
     * */
    public BD(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_REGISTROS + "(ID INTEGER PRIMARY KEY AUTOINCREMENT,Nombre TEXT,Apellido TEXT, FechaNacimiento TEXT, Altura TEXT,Correo TEXT,Contrasena TEXT)");
        db.execSQL("CREATE TABLE " + TABLE_CALCULOS + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_PERSONA TEXT,Fecha TEXT,Peso TEXT, MC TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REGISTROS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALCULOS);
        onCreate(db);
    }
}

