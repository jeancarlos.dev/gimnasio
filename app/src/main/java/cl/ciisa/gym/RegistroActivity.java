package cl.ciisa.gym;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
/**
 * @author Jean Carlos
 * @since 13-06-2021
 * */

public class RegistroActivity extends AppCompatActivity {
    private Button registroBtn, irLoginBtn;
    final Calendar fechaNacimiento = Calendar.getInstance();
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private EditText regNombre, regApellido, regCorreo, regContrasena, regFechaNac, regAltura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        openHelper = new BD(this);

        registroBtn = findViewById(R.id.btnRegLogin);
        irLoginBtn = findViewById(R.id.btnGotoLogin);
        regNombre = findViewById(R.id.etregNombre);
        regApellido = findViewById(R.id.etregApellido);
        regFechaNac = findViewById(R.id.etregFecNac);
        regAltura = findViewById(R.id.etregAltura);
        regCorreo = findViewById(R.id.etregCorreo);
        regContrasena = findViewById(R.id.etregContrasena);

        registroBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openHelper.getWritableDatabase();
                String fname = regNombre.getText().toString().trim();
                String fApellido = regApellido.getText().toString().trim();
                String fFechaNac = regFechaNac.getText().toString().trim();
                String fAltura = regAltura.getText().toString().trim();
                String fCorreo = regCorreo.getText().toString().trim();
                String fContrasena = regContrasena.getText().toString().trim();
                if (fname.isEmpty() || fApellido.isEmpty() || fFechaNac.isEmpty() || fAltura.isEmpty() || fContrasena.isEmpty() || fCorreo.isEmpty()) {
                    Toast.makeText(RegistroActivity.this, "Por favor llene todos los datos solicitados", Toast.LENGTH_SHORT).show();
                } else {
                    long id = insertData(fname, fApellido, fFechaNac, fAltura, fCorreo, fContrasena);
                    if (id > 0) {
                        Toast.makeText(RegistroActivity.this, "Usuario registrado satisfactoriamente", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent( RegistroActivity.this, MainActivity.class));
                        finish();
                    } else {
                        Toast.makeText(RegistroActivity.this, "Ops, ha ocurrido un problema al registrar al usuario", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        irLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistroActivity.this, MainActivity.class));
                finish();
            }
        });


        regFechaNac.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction())
                    new DatePickerDialog(RegistroActivity.this, date, fechaNacimiento
                            .get(Calendar.YEAR), fechaNacimiento.get(Calendar.MONTH),
                            fechaNacimiento.get(Calendar.DAY_OF_MONTH)).show();
                return false;
            }

        });
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            fechaNacimiento.set(Calendar.YEAR, year);
            fechaNacimiento.set(Calendar.MONTH, monthOfYear);
            fechaNacimiento.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };


    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        String fechaStr = sdf.format(fechaNacimiento.getTime());
        regFechaNac.setText(fechaStr);
        Toast.makeText(getApplicationContext(), "Fecha nacimiento seleccionada: " + fechaStr, Toast.LENGTH_SHORT).show();
    }


    public long insertData(String fnombre, String fApellido, String fFechaNac, String fAltura, String fcorreo, String fcontrasena) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BD.COL_2, fnombre);
        contentValues.put(BD.COL_3, fApellido);
        contentValues.put(BD.COL_4, fFechaNac);
        contentValues.put(BD.COL_5, fAltura);
        contentValues.put(BD.COL_6, fcorreo);
        contentValues.put(BD.COL_7, fcontrasena);

        return db.insert(BD.TABLE_REGISTROS, null, contentValues);
    }
}
