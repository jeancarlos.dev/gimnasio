package cl.ciisa.gym;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
/**
 * @author Jean Carlos
 * @since 13-06-2021
 * */

public class MainActivity extends AppCompatActivity {

    private TextView tvRegistrar;
    private EditText correo, contrasena;
    private Button loginButton;
    private SQLiteDatabase db;
    private SQLiteOpenHelper openHelper;
    private Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openHelper = new BD(this);
        db = openHelper.getReadableDatabase();
        tvRegistrar = findViewById(R.id.tvRegistrar);
        correo = findViewById(R.id.etEntCorreo);
        contrasena = findViewById(R.id.contrasena);
        loginButton = findViewById(R.id.btnLogin);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String correo = MainActivity.this.correo.getText().toString().trim();
                String contrasena = MainActivity.this.contrasena.getText().toString().trim();
                if (correo.isEmpty() || contrasena.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese su correo y contraseña para entrar", Toast.LENGTH_SHORT).show();
                } else {
                    cursor = db.rawQuery("SELECT *FROM " + BD.TABLE_REGISTROS + " WHERE " + BD.COL_6 + "=? AND " + BD.COL_7 + "=?", new String[]{correo, contrasena});
                    if (cursor != null) {
                        if (cursor.getCount() > 0) {
                            String nombre = "";
                            String apellido = "";
                            String altura = "";
                            int id = 0;
                            while (cursor.moveToNext()) {
                                int index;

                                index = cursor.getColumnIndexOrThrow(BD.COL_2);
                                nombre = cursor.getString(index);

                                index = cursor.getColumnIndexOrThrow(BD.COL_3);
                                apellido = cursor.getString(index);

                                index = cursor.getColumnIndexOrThrow(BD.COL_5);
                                altura = cursor.getString(index);

                                index = cursor.getColumnIndexOrThrow(BD.COL_1);
                                id = cursor.getInt(index);
                            }

                            Intent intent = new Intent(MainActivity.this, EntradaExitosa.class);
                            intent.putExtra("correo", correo);
                            intent.putExtra("nombre", nombre);
                            intent.putExtra("apellido", apellido);
                            intent.putExtra("altura", altura);
                            intent.putExtra("id", id);
                            Toast.makeText(getApplicationContext(), "Ingreso Exitoso", Toast.LENGTH_SHORT).show();
                            startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), "Error Ingreso. Usuario registrado?", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });


        tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RegistroActivity.class));
                finish();
            }
        });

    }
}
