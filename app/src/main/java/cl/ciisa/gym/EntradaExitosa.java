package cl.ciisa.gym;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
/**
 * @author Jean Carlos
 * @since 13-06-2021
 * */

public class EntradaExitosa extends AppCompatActivity implements RecordRecyclerViewAdapter.ItemClickListener {

    static RecordRecyclerViewAdapter adapter;
    final static ArrayList<Record> registros = new ArrayList<>();
    static String nombre;
    static String apellido;
    static String altura;
    static int idPer;
    final Calendar calculoCalendar = Calendar.getInstance();
    final Calendar fechaDesCalendar = Calendar.getInstance();
    final Calendar fechaHasCalendar = Calendar.getInstance();
    private EditText peso, fecha, fechaDesde, fechaHasta;
    private Button calcularButton;
    private SQLiteDatabase db;
    private SQLiteOpenHelper openHelper;
    private Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada_exitosa);


        Intent intent = getIntent();
        idPer = intent.getIntExtra("id", 0);
        nombre = intent.getStringExtra("nombre");
        apellido = intent.getStringExtra("apellido");
        altura = intent.getStringExtra("altura");
        TextView textView = (TextView) findViewById(R.id.tvdatosPer);
        textView.setText("Nombre: " + nombre + " " + apellido + " Altura: " + altura);


        openHelper = new BD(this);
        db = openHelper.getReadableDatabase();
        peso = findViewById(R.id.edtPeso);
        fecha = findViewById(R.id.etCalFecha);
        fechaDesde = findViewById(R.id.etDesdeFecha);
        fechaHasta = findViewById(R.id.etHastaFecha);
        calcularButton = findViewById(R.id.btnCalcular);


        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rvAnimals);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecordRecyclerViewAdapter(this, registros);
        adapter.setClickListener(this);
        //recyclerView.setLayoutManager(new GridLayoutManager(this, 3, GridLayout.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        cargarViewDesdeDB("SELECT * FROM " + BD.TABLE_CALCULOS + " WHERE " + BD.COL_CALCULOS_2 + "=?", new String[]{Integer.toString(idPer)});

        calcularButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                int id = intent.getIntExtra("id", 0);
                String idStr = Integer.toString(id);
                String pesoStr = EntradaExitosa.this.peso.getText().toString().trim();
                String fechaStr = EntradaExitosa.this.fecha.getText().toString().trim();
                if (pesoStr.isEmpty() || fechaStr.isEmpty()) {
                    Toast.makeText(EntradaExitosa.this, "Ingrese su peso actual para calcular", Toast.LENGTH_SHORT).show();
                } else {
                    cursor = db.rawQuery("SELECT * FROM " + BD.TABLE_REGISTROS + " WHERE " + BD.COL_1 + "=?", new String[]{idStr});
                    if (cursor != null) {
                        if (cursor.getCount() > 0) {
                            String alturaStr = "0";
                            while (cursor.moveToNext()) {
                                int index;
                                index = cursor.getColumnIndexOrThrow(BD.COL_5);
                                alturaStr = cursor.getString(index);
                            }

                            Calculo calculo;
                            Float altura = Float.parseFloat(alturaStr);
                            Float peso = Float.parseFloat(pesoStr);
                            calculo = new Calculo(altura, peso);
                            Float IMC = calculo.obtenerCalculo();
                            String imcStr = String.valueOf(IMC);

                            Record record = new Record();
                            record.fecha = fechaStr;
                            record.peso = pesoStr;
                            record.imc = imcStr;
                            record.setIdpersona(idStr);
                            record.setIdcalculo(insertData(idStr, fechaStr, String.valueOf(peso), imcStr));
                            registros.clear();
                            adapter.notifyDataSetChanged();
                            cargarViewDesdeDB("SELECT * FROM " + BD.TABLE_CALCULOS + " WHERE " + BD.COL_CALCULOS_2 + "=?", new String[]{Integer.toString(idPer)});
                            Toast.makeText(getApplicationContext(), "Registro Calculo Exitoso: " + imcStr, Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(getApplicationContext(), "Error al obtener datos usuario", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });


        fecha.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction())
                    new DatePickerDialog(EntradaExitosa.this, date, calculoCalendar
                            .get(Calendar.YEAR), calculoCalendar.get(Calendar.MONTH),
                            calculoCalendar.get(Calendar.DAY_OF_MONTH)).show();
                return false;
            }

        });

        fechaDesde.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction())
                    new DatePickerDialog(EntradaExitosa.this, dateDesde, fechaDesCalendar
                            .get(Calendar.YEAR), fechaDesCalendar.get(Calendar.MONTH),
                            fechaDesCalendar.get(Calendar.DAY_OF_MONTH)).show();
                return false;
            }

        });

        fechaHasta.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction())
                    new DatePickerDialog(EntradaExitosa.this, dateHasta, fechaHasCalendar
                            .get(Calendar.YEAR), fechaHasCalendar.get(Calendar.MONTH),
                            fechaHasCalendar.get(Calendar.DAY_OF_MONTH)).show();
                return false;
            }

        });


        fechaDesde.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateTablaFiltro();
            }
        });

        fechaHasta.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateTablaFiltro();
            }
        });


    }


    @Override
    public void onItemClick(View view, final int position) {

        final Record record = (Record) adapter.getItem(position);
        Intent intent = new Intent(EntradaExitosa.this, VerEliminarRecord.class);
        intent.putExtra("fecha", record.fecha);
        intent.putExtra("peso", record.peso);
        intent.putExtra("imc", record.imc);
        intent.putExtra("idCal", record.idcalculo);
        intent.putExtra("idPer", record.idpersona);
        intent.putExtra("position", position);
        startActivity(intent);


    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calculoCalendar.set(Calendar.YEAR, year);
            calculoCalendar.set(Calendar.MONTH, monthOfYear);
            calculoCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateCalculoFecha();
        }
    };


    DatePickerDialog.OnDateSetListener dateDesde = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            fechaDesCalendar.set(Calendar.YEAR, year);
            fechaDesCalendar.set(Calendar.MONTH, monthOfYear);
            fechaDesCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDesde();
        }
    };

    DatePickerDialog.OnDateSetListener dateHasta = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            fechaHasCalendar.set(Calendar.YEAR, year);
            fechaHasCalendar.set(Calendar.MONTH, monthOfYear);
            fechaHasCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateHasta();
        }
    };

    private void updateCalculoFecha() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        String fechaStr = sdf.format(calculoCalendar.getTime());
        fecha.setText(fechaStr);
        Toast.makeText(getApplicationContext(), "Fecha calculo seleccionada: " + fechaStr, Toast.LENGTH_SHORT).show();
    }


    private void updateDesde() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        String fechaStr = sdf.format(fechaDesCalendar.getTime());
        fechaDesde.setText(fechaStr);
        //updateTablaFiltro();
        Toast.makeText(getApplicationContext(), "Fecha Desde seleccionada: " + fechaStr, Toast.LENGTH_SHORT).show();
    }

    private void updateHasta() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        String fechaStr = sdf.format(fechaHasCalendar.getTime());
        fechaHasta.setText(fechaStr);
        //updateTablaFiltro();
        Toast.makeText(getApplicationContext(), "Fecha Hasta seleccionada: " + fechaStr, Toast.LENGTH_SHORT).show();
    }


    private void updateTablaFiltro() {

        String desdeFechaStr = EntradaExitosa.this.fechaDesde.getText().toString().trim();
        String hastaFechaStr = EntradaExitosa.this.fechaHasta.getText().toString().trim();
        if (!desdeFechaStr.isEmpty() && !hastaFechaStr.isEmpty()) {
            registros.clear();
            adapter.notifyDataSetChanged();
            cargarViewDesdeDB("SELECT * FROM " + BD.TABLE_CALCULOS + " WHERE " + BD.COL_CALCULOS_3 + " BETWEEN " + "'" + desdeFechaStr + "' AND '" + hastaFechaStr + "'" + " AND " + BD.COL_CALCULOS_2 + "=?", new String[]{Integer.toString(idPer)});
        } else if (!desdeFechaStr.isEmpty() && hastaFechaStr.isEmpty()) {
            registros.clear();
            adapter.notifyDataSetChanged();
            cargarViewDesdeDB("SELECT * FROM " + BD.TABLE_CALCULOS + " WHERE " + BD.COL_CALCULOS_3 + " BETWEEN " + "'" + desdeFechaStr + "' AND '" + obtenerFechaActual() + "'" + " AND " + BD.COL_CALCULOS_2 + "=?", new String[]{Integer.toString(idPer)});

        } else if (desdeFechaStr.isEmpty() && !hastaFechaStr.isEmpty()) {
            registros.clear();
            adapter.notifyDataSetChanged();
            cargarViewDesdeDB("SELECT * FROM " + BD.TABLE_CALCULOS + " WHERE " + BD.COL_CALCULOS_3 + " BETWEEN " + "'" + obtenerFechaActual() + "' AND '" + hastaFechaStr + "'" + " AND " + BD.COL_CALCULOS_2 + "=?", new String[]{Integer.toString(idPer)});

        } else {
            registros.clear();
            adapter.notifyDataSetChanged();
            cargarViewDesdeDB("SELECT * FROM " + BD.TABLE_CALCULOS + " WHERE " + BD.COL_CALCULOS_2 + "=?", new String[]{Integer.toString(idPer)});
        }

    }

    public String obtenerFechaActual() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return df.format(c);
    }


    public String insertData(String ID_PERSONA, String fecha, String peso, String imc) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BD.COL_CALCULOS_2, ID_PERSONA);
        contentValues.put(BD.COL_CALCULOS_3, fecha);
        contentValues.put(BD.COL_CALCULOS_4, peso);
        contentValues.put(BD.COL_CALCULOS_5, imc);
        return String.valueOf(db.insert(BD.TABLE_CALCULOS, null, contentValues));
    }


    private void cargarViewDesdeDB(String query, String[] parameters) {
        cursor = db.rawQuery(query, parameters);
        if (cursor != null) {

            if (cursor.getCount() > 0) {
                String fecha = "";
                String peso = "";
                String imc = "";

                while (cursor.moveToNext()) {

                    fecha = cursor.getString(2);
                    peso = cursor.getString(3);
                    imc = cursor.getString(4);
                    Record record = new Record();
                    record.fecha = fecha;
                    record.peso = peso;
                    record.imc = imc;
                    record.idcalculo = cursor.getString(0);
                    record.idpersona = cursor.getString(1);

                    registros.add(record);
                    adapter.notifyItemInserted(registros.size() - 1);
                }
            } else {
                Toast.makeText(getApplicationContext(), "Lista de calculos Vacia", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
