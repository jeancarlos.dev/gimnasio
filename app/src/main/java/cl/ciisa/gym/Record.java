package cl.ciisa.gym;

import java.util.ArrayList;
/**
 * @author Jean Carlos
 * @since 13-06-2021
 * */

public class Record {

    public String fecha;
    public String peso;
    public String imc;
    public String idpersona;
    public String idcalculo;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getImc() {
        return imc;
    }

    public void setImc(String imc) {
        this.imc = imc;
    }

    public String getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(String idpersona) {
        this.idpersona = idpersona;
    }

    public String getIdcalculo() {
        return idcalculo;
    }

    public void setIdcalculo(String idcalculo) {
        this.idcalculo = idcalculo;
    }

    public static int getLastContactId() {
        return lastContactId;
    }

    public static void setLastContactId(int lastContactId) {
        Record.lastContactId = lastContactId;
    }


    private static int lastContactId = 0;

    public static ArrayList<Record> createContactsList(Record record) {
        ArrayList<Record> contacts = new ArrayList<Record>();
        contacts.add(record);
        return contacts;
    }
}
